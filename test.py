#!/usr/bin/env python2.7

__unittest = True

import subprocess
import os
import unittest
import xmlrunner
from subprocess import CalledProcessError, Popen, PIPE

class LinkedDictionaryTest(unittest.TestCase):
    def make_file (self, target):
        FNULL = open(os.devnull, 'w')
        self.assertEqual(subprocess.call(['make', target], stdout=FNULL, stderr=FNULL), 0, "Something went wront with make " + target)
    
    def launch (self, file, input):
        output = b''
        err = b''
        try:
            p = Popen(['./'+file], shell=None, stdin=PIPE, stdout=PIPE, stderr=PIPE)
            (output, err) = p.communicate(input.encode())
            self.assertNotEqual(p.returncode, -11, 'segmentation fault')
            return (output.decode(), err.decode(), p.returncode)
        except CalledProcessError as exc:
            self.assertNotEqual(exc.returncode, -11, 'segmentation fault')
            return (exc.output.decode(), exc.returncode)
        
    def test_existing_keys(self):
        self.make_file('main')
        values = dict()
        values['Hello'] = "There is something you must know"
        values['Today'] = "Magic world around us"
        values['Fourth key'] = "All programming languages have a notion of evaluation strategy. It describes the order of evaluation in complex expressions. How should we evaluate f (g(1), h(4))? Should we evaluate g(1) and h(4) first and then let f act on the results? Or should we inline g(1) and h(4) inside the body of f and defer their own evaluations until they are really needed?"
        for key in values:
            (output, err, code) = self.launch('main', key)
            self.assertEqual(output, values[key], "find_word prints wrong message for key(%s): expected output(%s), real output(%s)" % (key, values[key], output))
    
    def test_not_existing_keys(self):
        self.make_file('main')
        values = dict()
        values['hello'] = "Error: the line doesn't match any key word.\n"
        values['hey hey hey'] = "Error: the line doesn't match any key word.\n"
        for key in values:
            (output, err, code) = self.launch('main', key)
            self.assertEqual(code, 2, "find_word should have failed on key(%s), but printed (%s)" % (key, repr(err)))
            self.assertEqual(err, values[key], "find_word prints wrong error for key(%s): expected output(%s), real output(%s)" % (key, values[key], repr(err)))

    def test_too_long_keys(self):
        self.make_file('main')
        values = dict()
        values["""All programming languages have a notion of evaluation strategy. It describes the order of evaluation in complex expressions. How should we evaluate f (g(1), h(4))? Should we evaluate g(1) and h(4) first and then let f act on the results? Or should we inline g(1) and h(4) inside the body of f and defer their own evaluations until they are really needed?"""] = 'Error: the line was not read correctly. Buffer overflow captured.\n'
        for key in values:
            (output, err, code) = self.launch('main', key)
            self.assertEqual(code, 1, "find_word should have failed on key(%s), but printed (%s)" % (key, repr(err)))
            self.assertEqual(err, values[key], "find_word prints wrong error for key(%s): expected output(%s), real output(%s)" % (key, values[key], repr(err)))



if __name__ == "__main__":
    with open('report.xml', 'w') as report:
        unittest.main(testRunner=xmlrunner.XMLTestRunner(output=report), failfast=False, buffer=False, catchbreak=False)
