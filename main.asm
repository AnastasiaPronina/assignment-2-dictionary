%include "words.inc"
%include "dict.inc"
%include "lib.inc"
%include "exit_codes.inc"

%define buf_size 256
%define end_line 0xA
%define null 0

section .rodata
f_error:
    db "Error: the line doesn't match any key word.", end_line, null
r_error:
    db "Error: the line was not read correctly. Buffer overflow captured.", end_line, null

section .bss
bufer:
    resb buf_size

section .text
global _start

_start:
    mov rdi, bufer
    mov rsi, buf_size
    call read_line
    test rax, rax
    jz .read_error
    mov rdi, bufer
    mov rsi, begin_ref
    call find_word
    test rax, rax
    jz .find_error
    mov rdi, [rax + dict_value_ptr_offset]
    call print_string
    xor rdi, rdi
    jmp exit
.read_error:
    mov rdi, r_error
    call print_error
    mov rdi, reading_error
    jmp exit
.find_error:
    mov rdi, f_error
    call print_error
    mov rdi, search_error
    jmp exit
