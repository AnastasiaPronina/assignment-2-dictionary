.PHONY: all clear test

ASM=nasm
flags=-f
fdef=elf64

main.o: main.asm lib.inc words.inc dict.inc
	${ASM} $< ${flags} ${fdef} -o $@

lib.o: lib.asm
	${ASM} $< ${flags} ${fdef} -o $@

dict.o: dict.asm lib.inc
	${ASM} $< ${flags} ${fdef} -o $@

main: main.o lib.o dict.o
	ld -o $@ $^

clear:
	rm main main.o lib.o dict.o

test:
	./test.py
