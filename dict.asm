%include "lib.inc"
%include "colon.inc"

section .text
global find_word

; Принимает указатель на нуль-терминированную строку (rdi) и адрес начала словаря (rsi)
; Возвращает указатель на элемент связного списка, содержащий нужное вхождение
; Если вхождение не найдено, возвращает 0
find_word:
    push r12
    push r13
    mov r12, rsi
    mov r13, rdi
    .loop:
        test r12, r12
        jz .not_found
        mov rdi, [r12 + dict_key_ptr_offset]
        mov rsi, r13
        call string_equals
        test rax, rax
        jnz .found
        mov r12, [r12]
        jmp .loop
    .found:
        mov rax, r12
        jmp .end
    .not_found:
        xor rax, rax
        jmp .end
    .end:
        pop r13
        pop r12
    ret



