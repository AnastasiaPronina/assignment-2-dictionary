%define begin_ref 0x0
%define dict_key_ptr_offset 8
%define dict_value_ptr_offset 16

; макрос принимает на вход ключевую строку и название метки, по которой сохранять соответствующее значение
; сначала идет адрес следующей пары (текущее х1)
; потом идет адрес, по которому сохраняется ключевая строка
; далее адрес, по которому сохраняется значение

%macro colon 2
    %ifstr %1
        %ifid %2
            %%key:
                db %1, 0
            %%element:
                dq begin_ref
                dq %%key
                dq %2
            %2:
                %define begin_ref %%element
        %else
            %error "Second argument should be a label"
        %endif
    %else
        %error "First argument should be a string"
    %endif
%endmacro

; Макрос принимает на вход произвольное количество аргументов (предполагается, что это строк(а/и)).
; Проверяет, является ли ввод нуль-терминированной строкой. Если нет, дописывает в конец 0.
; Предупреждает ошибку при чтении не нуль-терминированной строки.
%macro db 1-*
    %rep %0
        db %1
        %rotate 1
    %endrep
    %rotate -1
    %ifstr %1
        %ifnidn %1, "\0"
            db 0
        %endif
    %else
        %if %1 <> 0
            db 0
        %endif
    %endif
%endmacro

