%include "colon.inc"

section .rodata

colon "Hello", world
db "There is something you must know", 0

colon "Goodbye", nasty
db "Another word"

colon "Today", magic
db "Magic world around us"

colon "Fourth key", label
db "All programming languages have a notion of evaluation strategy. It describes the order of evaluation in complex expressions. How should we evaluate f (g(1), h(4))? Should we evaluate g(1) and h(4) first and then let f act on the results? Or should we inline g(1) and h(4) inside the body of f and defer their own evaluations until they are really needed?"

colon "first label", label_1
db "There is some text for label 1"

colon "second label", label_2
db "There is some text for label 2"

colon "third label", label_3
db "There is some text for label 3"

colon "fifth label", label_5
db "There is some text for label 5"
